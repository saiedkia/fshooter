﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjects : MonoBehaviour
{
    int _live = 3;
    public int Live
    {
        get { return _live; }
        set { _live = value; if (value == 0) Boom(); }
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        print(collision.gameObject.name);
        if (collision.gameObject.tag != "bullet") { return; }
        //GameObjects gameObjects = collision.gameObject.GetComponent<GameObjects>();
        Live -= 1;
        Destroy(collision.gameObject);
    }

    private void Boom()
    {
        Vector3 position = transform.position;
        Destroy(this.gameObject);


         GameObject bulletObject = Instantiate((GameObject)Resources.Load("Boom"), position, new Quaternion(0, 0, 0, 0));
    }
}
