﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{


    Transform _transform;
    SpriteRenderer _renderer;
    Vector3 _width;
    bool _ltrMovment;

    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        _renderer = GetComponent<SpriteRenderer>();

        _width = Camera.main.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(_transform.position.x >= 5)
        {
            _ltrMovment = false;
            _renderer.flipX = true;
        }
        else if (_transform.position.x <= -5)
        {
            _ltrMovment = true;
            _renderer.flipX = false;
        }

        _transform.position = new Vector3(_transform.position.x + (_ltrMovment ? Time.deltaTime : -Time.deltaTime)  * 3, _transform.position.y, 0);
    }
}