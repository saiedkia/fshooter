﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResetGame : MonoBehaviour
{
    public Button PlayPauseButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void ShowMenu()
    {

    }



    public void PlayPause()
    {
        if (Time.timeScale >= 1)
        {
            Time.timeScale = 0;
            ((Text)(PlayPauseButton?.GetComponentInChildren(typeof(Text)))).text = "play";
        }
        else
        {
            Time.timeScale = 1;
            ((Text)(PlayPauseButton?.GetComponentInChildren(typeof(Text)))).text = "pause";
        }

    }
}
