﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Charecter : MonoBehaviour
{
    public bool MoveCameraByCharacter;
    public void Shooting()
    {
        GameObject bulletObject = Instantiate((GameObject)Resources.Load("bullet"), transform.position, new Quaternion(0, 0, 0, 0));
        Bullet bullet = bulletObject.GetComponent<Bullet>();
        bullet.Speed = 5;
        bullet.RightToLeft = _renderer.flipX;



        Collider2D bulletCollider = bullet.GetComponent<Collider2D>();
        Physics2D.IgnoreCollision(bulletCollider, GetComponent<Collider2D>());

        Destroy(bulletObject, 5.0f);
    }

    CharecterState _state = CharecterState.Idle;
    SpriteRenderer _renderer;
    Animator _animator;
    Rigidbody2D _ridgidBody;
    Transform _transform;
    SpriteRenderer _ammo;

    Vector3 cameraOffset;
    const int force = 2;
    bool isGrounded = true;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        _ridgidBody = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();

        cameraOffset = Camera.main.transform.position - _transform.position;
        cameraOffset.y -= _transform.position.y - (_transform.position.y / 3);
    }

    // Update is called once per frame
    void Update()
    {
        _state = CharecterState.Idle;
        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.DownArrow))
                _state = CharecterState.Crouch;

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                _state = CharecterState.MoveLeft;
                _renderer.flipX = true;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                _state = CharecterState.MoveRight;
                _renderer.flipX = false;
            }


            if (Input.GetKey(KeyCode.UpArrow)){
                _state = CharecterState.Jump;
            }
            else if (Input.GetKey(KeyCode.Space))
                _state &= CharecterState.Shooting;



            //if (Input.GetKey(KeyCode.S))
            //{
            //    Shoot();
            //}
        }
        else
        {
            _state = CharecterState.Idle;
        }

        MoveCharacter();
        PlayAnimatins();
        UpdateCameraPosition();


    }

    void PlayAnimatins()
    {
        switch (_state)
        {
            case CharecterState.Crouch:
                {
                    _animator.Play("crunch");
                    break;
                }
            case CharecterState.Jump:
            case CharecterState.MoveRight | CharecterState.Jump:
            case CharecterState.MoveLeft | CharecterState.Jump:
            case CharecterState.Idle:
                {
                    _animator.Play("stand");
                    break;
                }
            case CharecterState.Crouch & CharecterState.Shooting:
                {
                    if(isGrounded)
                    {
                        _animator.Play("crunchShooting");
                       // Shoot(10);
                    }
                    break;
                }
            case CharecterState.MoveRight:
            case CharecterState.MoveLeft:
                {
                    _animator.Play("walk");
                    break;
                }
            case CharecterState.MoveRight & CharecterState.Shooting:
            case CharecterState.MoveLeft & CharecterState.Shooting:
                {
                    if (isGrounded)
                    {
                        _animator.Play("standShoot");
                        //Shoot(30);
                    }
                    break;
                }
        }
    }

    void MoveCharacter()
    {
        Vector3 oldPosition = transform.position;

        if((_state & CharecterState.MoveLeft) == CharecterState.MoveLeft)
        {
            transform.position = new Vector3(oldPosition.x - Time.deltaTime * force, oldPosition.y, 0);
            //_ridgidBody.AddForce(new Vector2(-10 * force, 0));
        }else if ((_state & CharecterState.MoveRight) == CharecterState.MoveRight)
        {
            transform.position = new Vector3(oldPosition.x + Time.deltaTime * force, oldPosition.y, 0);
            //_ridgidBody.AddForce(new Vector2(10 * force, 0));
        }

        if((_state & CharecterState.Jump) == CharecterState.Jump)
        {
            if (isGrounded)
            {
                _ridgidBody.AddForce(new Vector2(0, 120 * 3));
                isGrounded = false;
            }
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "bulkyGuy")
        {
            // just for testing 
            // load scene two   
            SceneManager.LoadScene("SceneTwo", LoadSceneMode.Single);
           
        }else if (collision.gameObject.name == "ground")
        {
            isGrounded = true;
        }
    }



    private void UpdateCameraPosition()
    {
        if (MoveCameraByCharacter)
        {
            float oldY = Camera.main.transform.position.y;
            Vector3 position = _transform.position + cameraOffset;
            position.y = oldY;
            Camera.main.transform.position = position;
        }
    }


    //private void Shoot(int delay = 0)
    //{
        //var x = _animator.runtimeAnimatorController.animationClips;
        //x[0].len
        //// SceneManager.GetActiveScene()
        //if(_animation["crunchShooting"].time <= 0.5)
        //{
        //    GameObject bullet = Instantiate((GameObject)Resources.Load("bullet"), transform.position, new Quaternion(0, 0, 0, 0));
        //}

    //}
}

[Flags]
enum CharecterState
{
    Idle =      1,
    Jump =      2,
    MoveRight = 49,
    MoveLeft =  8,
    Shooting =  116,
    Crouch =    321
}
