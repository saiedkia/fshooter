﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int Speed = 60;
    public bool RightToLeft;
    // Start is called before the first frame update
    void Start()
    {
        tag = "bullet";
    }

    // Update is called once per frame
    void Update()
    {
        if (RightToLeft)
            transform.position = new Vector3(transform.position.x - Time.deltaTime * Speed * 2, transform.position.y, 0);
        else
            transform.position = new Vector3(transform.position.x + Time.deltaTime * Speed * 2, transform.position.y, 0);
    }

}
