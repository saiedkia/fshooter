﻿using UnityEngine;

public class GameCanvas : MonoBehaviour
{
    void Awake()
    {
        GameObject canvas = (GameObject)Resources.Load("Canvas");
        //gameObject.AddComponent()
        Canvas c = canvas.GetComponent<Canvas>();
        c.enabled = true;
        Instantiate(canvas);
    }
}
